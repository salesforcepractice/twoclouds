public class TC_AddContactToCallBtnController {
  @AuraEnabled
  public static void addContactToCall(Id contactId, String interactionId) {
    Task t = [
      SELECT  Id 
      FROM    Task
      WHERE   CallObject = :interactionId
      LIMIT 1
    ];
    
    t.WhoId = contactId;
    update t;
  }
}
