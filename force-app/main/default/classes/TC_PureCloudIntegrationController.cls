public class TC_PureCloudIntegrationController {
  @AuraEnabled
  public static string getCallCenterUrl() {
    String url = '';
    String userId = UserInfo.getUserId();

    User userRecord = [
      SELECT CallCenterId 
      FROM User 
      WHERE Id =: userId 
      LIMIT 1
    ];
    
    if (String.isNotBlank(userRecord.CallCenterId)) {
        CallCenter callCenterRecord = [
          SELECT AdapterUrl
          FROM CallCenter
          WHERE Id = :userRecord.CallCenterId
          Limit 1
        ];
        if (String.isNotBlank(callCenterRecord.AdapterUrl)) {
          url = callCenterRecord.AdapterUrl;
        }
    } 
    else {
      throw new AuraHandledException('No Call Center is defined for the User.');
    }
    
    return url;
  }
}
