({
  handleClick : function(component, event, helper) {
    var interactionId = sessionStorage.getItem("TC_PureCloudIntegration_interactionId");
    component.set("v.debugMsg", interactionId);
    if (!interactionId || interactionId === "") {
      component.set("v.displayMsg", "No active calls at the moment.");
      return;
    }

    var action = component.get("c.addContactToCall");
    action.setParams({
      contactId : component.get("v.recordId"),
      interactionId : interactionId
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        component.set("v.displayMsg", "Contact attached to call with interaction id: " + interactionId);
      }
      else if (state === "ERROR") {
        var errorMsg = "Failed to attach Contact to call with interaction id: " + interactionId;
        var errors = response.getError();
        if (errors && errors[0] && errors[0].message) {
          errorMsg += "\n" + "Error message: " + response.getError()[0].message;
        }
        else {
          errorMsg += "\n" + "(Unknown error)";
        }
        component.set("v.displayMsg", errorMsg);
      }
      else {
        var errorMsg = "Failed to attach Contact to call with interaction id: " + interactionId;
        errorMsg += "\n" + "(Unknown error)";
        component.set("v.displayMsg", errorMsg);
      }
    });

    $A.enqueueAction(action);
  }
})
