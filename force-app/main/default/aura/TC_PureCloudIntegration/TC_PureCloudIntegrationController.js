({
  doInit : function(component, event, helper) {
    var action = component.get("c.getCallCenterUrl");
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var returnedUrl = response.getReturnValue();
        var clientOrigin = returnedUrl.match(/^(http(s?):\/\/[^\/]+)/gi)[0];
        component.set('v.clientOrigin', clientOrigin);

        window.addEventListener("message", $A.getCallback(function(event) {
          if (event.origin != clientOrigin) {
            return;
          }
                                            
          if (event.source && event.data && event.data.type && event.data.type === 'Interaction') {
            var interactionId = event.data.data.id;
            if (event.data.category === 'connect') {
              var message = JSON.stringify(event.data);
              component.set("v.debugMsg", message);
              component.set("v.interactionId", interactionId);
              window.sessionStorage.setItem("TC_PureCloudIntegration_interactionId", interactionId);
            } 
            if (event.data.category === 'acw') {
              var savedInteractionId = component.get("v.interactionId");
              var message = JSON.stringify(event.data);
              component.set("v.debugMsg", message);
              
              if (savedInteractionId && savedInteractionId != "" && savedInteractionId === interactionId) {
                component.set("v.interactionId", "");
                window.sessionStorage.removeItem("TC_PureCloudIntegration_interactionId");
              }
            }
          }
        }), false);
      }
    });

    $A.enqueueAction(action);
  }
})
